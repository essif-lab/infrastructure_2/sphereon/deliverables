# Presentation Exchange - Credential Query - Feasibility Report

# Table of Contents
- [Background](#background)
- [Key accomplishments](#key-accomplishments)
- [Deviations and takeaways](#deviations-and-takeaways)
- [Integrations and External Parties](#integrations-and-external-parties)
- [Interoperability](#interoperability)
- [Standardization](#standardization)
- [Deliverables](#deliverables)
    * [Specifications](#specifications)
    * [Source code](#source-code)
- [Dissemination](#dissemination)
- [Final words](#final-words)

# Background

[Sphereon](https://sphereon.com), as part of its Vindicatio project, has created an implementation of the [DIF Presentation Exchange V1 and V2](https://identity.foundation/presentation-exchange/) (PEX) for DIF/W3C SSI solutions. PEX is compatible with Aries Present Proof Protocol v2 and OpenID Connect 4 Verifiable Presentations, and uses a layered approach to achieve both integration and interoperability.
At the lowest layers there are TypeScript (Javascript) libraries that allow for easy integration in other projects for both web and mobile. 
The PEX OpenAPI specification and generator allows the models to be generated for any programming language of choice. 
Then there is the REST API which exposes the functionality of PEX to be used in any programming language. 
Lastly our PEX library is integrated as part of a Self Issued OpenID Connect v2 (SIOPv2) and OpenID Connect 4 Verifiable Presentations (OIDC4VP) library.


# Key accomplishments



| Goal                                                                                                                            | Achievement against set goals                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
|---------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Create and OpenAPI specification for the DIF PEX v1 specification allowing to generate models for multiple programing languages | In [PEX&#x2011;OpenAPI](https://github.com/Sphereon-Opensource/pex-openapi) we created am OpenAPI Specification for V1. Next to that we added V2 Presentation Exchange support, which is still an evolving spec at DIF. We can generate source-code for many target programming languages of the models/data-structures                                                                                                                                                                                    |
| Create a typescript library for direct integration into wallets and verifier systems                                            | The [PEX&#x2011;Typescript](https://github.com/Sphereon-Opensource/pex) library, uses models from PEX-OpenAPI. It has seen 750 commits, by 8 developers and 15 releases, before reaching a stable version for V1. We added PEX spec v2 support as well. Developers can explicitly use v1 or v2, or decide to allow the libary to do feature detection and choose the correct version automatically. As far as we know this is the only Open-Source implementation implementing 100% of the specifications. |
| Create a REST API with state machine, to allow non-typescript systems to integrate                                              | [PEX&#x2011;MS](https://github.com/Sphereon-Opensource/pex-ms) allows people to run a Microservice that exposes an API and integrates with an ORM/Database system (mongo for now). It can create sessions to interact between a wallet/app and a verifier system. It uses sessions and challenge-response as part of the interaction. PEX-MS can be run as a docker container if desired.                                                                                                                  |
| Aim to integrate with DIDCommv2/CHAPI                                                                                           | We deviated here. See the next chapter for more details. Instead we focused on something which initially was left out of scope: OIDC4VP and SIOPv2 support in [SIOPv2&#160;&&#160;OIDC4VP](https://github.com/Sphereon-Opensource/did-auth-siop), allowing trustless exchange of credentials using a standards based protocol (OpenID) and our Presentation Exchange                                                                                                                                       |

# Deviations and takeaways
We grossly underestimated the complexity of the V1 Presentation Exchange specification. The V2 specification emerged last few months and is still being worked on. It is a simplification over V1, and now active discussions are ongoing to create profiles for the specification, because it is highly unlikely that all wallets/verifiers will be capable in supporting the full specification.
Our main PEX library has seen development by 8 developers and a total of almost 750 commits over 9 months, resulting in 15 releases, before we reached our first stable version. That is a factor 2-3 more over that period than we anticipated. A small part of that has to do with the decision to also support version 2 of the specification, as we believe that will be the de facto version for any implementation.

Initially we left OpenID Connect integration out of scope because of the size of this undertaking. We were aiming to create DIDCommv2/CHAPI integration. With Animo Solutions integrating our PEX library for Aries Present Proof v2 and DIDComm v2 still being actively developed and at the same time EBSI moving towards SIOPv2/OIDC4VP, Gimly and Sphereon having another project for SIOPv2/OIDC4VP and lastly Gataca asking us in scope of the VUI project, we decided it made more sense to focus on SIOPv2/OIDC4VP integration and thus not integrating Aries/DIDCommv2/CHAPI. It will be part of the work Animo is doing.
OpenID (Connect), an extension on top of Oauth2, is the de facto standard for Authentication/Authorization in enterprise en webapp contexts. Given the OpenID Foundation is embracing the Presentation Exchange and creating specification for the actual integration, we decided that this would be more worthwhile, despite the size of this work. Together with Gimly we created an Open-Source library that is the first SIOPv2/OIDC4VP library that implements the OpenID specifications and has 100% Presentation Exchange support.

# Integrations and external parties

Below is a list of parties and project that either are using Sphereon's PEX solution, or that expressed interest to integrate once PEX reached its first stable version (which happened on 13-01-2022).

### Sphereon

Sphereon is using PEX in several solutions. First there is its Verifiable Data Platform, which is about trustworthy verifiable data that can range from full SSI to managed and federated identities. PEX is being integrated into its verifier architecture.

Next there are Open-Source SSI SDKs and a Wallet. Both are integrating with PEX to allow standardization and interoperability with external systems and other P2P users.

### Gimly
Gimly is a Dutch SSI company and eSSIF-Lab infrastructure 2 call participant, which is building an SSI ID/Wallet solution. Gimly has integrated our PEX library as part of its commercial Gimly ID application, leveraging Presentation Exchange through Self-Issued OpenID Connect and OpenID Connect for Verifiable Presentations.

### Animo Solutions
Animo Solutions is a Dutch SSI development company and participating in the eSSIF-Lab Infrastructure 3 call. Being practically neighbours in the Netherlands, means we also have short communication lines with them.
They are integrating our PEX in their Aries Mobile SDK, which is a React-Native SDK targeting mobile solutions using Aries based protocols. They will be using PEX for the Aries Present Proof v2 Protocol.

### Gataca - Verifier Universal Interface
Gataca is Spanish SSI development company and participating in the eSSIF-Lab Infrastructure 2 call. We have been working with them as part of the Verifier Universal Interface project. They are integrating PEX in their solutions. 

### Credenco
Credenco is a eSSIF-Lab sub-grantee with a commercial SSI solution with products for instance for identities of workers and civil servants. It is also working closely together with the Dutch Justice department on a solution for digital Certificates of Good Conduct.
Credenco is integrating PEX in its mobile app and verifier system to allow its customers to make requirements on acceptable credentials.

### Firm24 & LTO Network
Firm24, blockchain LTO Network and Sphereon are involved in a Dutch Company Passport project. Firm24 incorporates a large percentage of new Dutch companies, by having a digital platform integrated with the Chamber of Commerce and notaries. As part of incorporation, board members and Ultimate Beneficiary Owners will have a SSI wallet, containing credentials. Our PEX library helps external parties and Firm24 to pose requirements on exchange of credentials.

### Consensys Mesh - Veramo framework (former uPort)
The Open-Source Veramo framework expressed interest in integrating Sphereon's PEX in its framework. Veramo is an SSI framework created by former uPort, now part of Consensys, to easily integrate agent functionality in mobile, backend and web apps. The framework is the result of lessons learned by being a pioneer in the SSI space for many years. Veramo and Sphereon have been and are in discussion about the integrations. Now that we have reached our first stable version, the actual collaboration to get it integrated will start.

### Other parties
Sphereon is in talks with other commercial parties that have expressed interest in using PEX, ranging from IT companies, eClinical Trial vendors, to HR companies, government, transportation and even Casinos. Given the nature and state of these talks we cannot disseminate more unfortunately at this point in time.



# Interoperability

We decided to take part in the Verifier Universal Interface (VUI) project which was started by Gataca. 
Sphereon's part in that project was to deliver the Presentation Exchange support, as well as to create the test suite to showcase interop between Sphereon's API and Gataca's API.

Sphereon is passing the tests in the testsuite, which is hosted in the [eSSIF-Lab Gitlab](https://gitlab.grnet.gr/essif-lab/interoperability/WP-VUI-PresentationExchange)

Given the interest in our library and that it bridges the gap betwene verifiers and wallets, the library and supporting implementations will aid in interoperability at large, as it is agnostic with regards to the encapsulating transport. Animo integrating it in Aries Present Proof v2, and Gimly/Sphereon integrating it in OpenID Connect is a testament to that.

The work done under eSSIF-Lab in the VUI project is being continued in the Decentralized Identity Foundation.

## Donation to Decentralized Identity Foundation
Sphereon's management decided to donate the PEX library and PEX openapi work to the Decentralized Identity Foundation. The handover will happen later this quarter. This should help with adoption and standardisation even more.

# Standardization

In order to support standardization we participated both in the Decentralized Identity Foundation Presentation Exchange, WACI-PEX and OpenID Foundation meetings and provided input and feedback, both on the Presentation Exchange specification as well as on the Self-Issued OpenID Connect and OpenID Connect 4 Verifiable Presentations specification, the latter incorporates Presentation Exchange.
One example in the OpenID Foundations Bitbucket: [Comments on SIOPV2 & OIDC4VP](https://bitbucket.org/openid/connect/pull-requests/61/incorporating-sphereons-comments?link_source=email))

Next we have ensured that we have a 100% conforming implementation of the Presentation Exchange, as far as we know the only one in existence to achieve that at this point. When work on Version 2 started for the Presentation Exchange we decided to add support ot our library as well. Users can choose to explicitly support V1 or V2 of the specification, or use feature detection, where the library determines itself which version is being processed.


# Deliverables

## Specifications

The eSSIF-Lab specifications are published in the [eSSIF-Lab Gitlab](https://gitlab.grnet.gr/essif-lab/infrastructure_2/sphereon/deliverables). The release status of all the key project deliverables is summarised in the table below.
Please note that more up-to-date documentation is available in the respective source-code repositories mentioned below, to keep them in a single place.

| Milestone        | Date      | Deliverables                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|------------------|-----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Milestone&#160;1 | 15&#x2011;05&#x2011;2021 | - [Functional Specification](https://gitlab.grnet.gr/essif-lab/infrastructure_2/sphereon/deliverables/-/blob/master/functional_specifications/README.md) <br/>- [Interface Specification](https://gitlab.grnet.gr/essif-lab/infrastructure_2/sphereon/deliverables/-/blob/master/interface_specifications/README.md) <br/>- [Envisioned Interoperability](https://gitlab.grnet.gr/essif-lab/infrastructure_2/sphereon/deliverables/-/blob/master/interoperability/envisioned_interoperability_with_others.md) |
| Milestone&#160;2 | 15&#x2011;01&#x2011;2022 | - Source code, releases and documentation (see below)<br/>- Feasibility report (this document)                                                                                                                                                                                                                                                                                                                                                                                                                |


## Source code

The below GitHub links point to Apache-2 license source-code developed as part of the eSSIF-Lab project. They are all targeted at integration by external developers and thus not reliant on any of our commercial and/or proprietary solutions.
The repositories contain extensive developer and integrator documentation, including flow & class diagrams. We decided to not host them in the eSSIF-Lab Gitlab anymore because it meant we needed to track multiple locations and everything being open-source future development activities will happen in our Github anyway.

| Repository                                                                         | Description                                                                                                                                                                                                                            |
|------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [PEX&#x2011;OpenAPI](https://github.com/Sphereon-Opensource/pex-openapi)           | OpenAPI Specification for V1 and V2 Presentation Exchange. Allowing source-code generation for many target programming languages of the models/data-structures                                                                         |
| [PEX&#x2011;Typescript](https://github.com/Sphereon-Opensource/pex)                | Typescript library, using PEX-OpenAPI, with only a few dependencies, that implements the full V1 and V2 DIF Presentation Exchange specification                                                                                        |
| [PEX&#x2011;Examples](https://github.com/Sphereon-Opensource/pex-consumer-example) | Example Credentials, Verifiable Presentations found in the wild. We created PEX definitions to test our libraries with different Credentials and Presentations                                                                         |
| [PEX&#x2011;MS](https://github.com/Sphereon-Opensource/pex-ms)                     | REST API, that allows Presentation Exchange interactions with statefull persistence, allowing integration for non-typescript systems and server/agent based interactions. Can also be run as a Docker container                        |
| [SIOPv2&#160;&&#160;OIDC4VP](https://github.com/Sphereon-Opensource/did-auth-siop) | Self Issued OpenID Connect and OpenID Connect 4 Verifiable Presentations library, allowing peer 2 peer authentication removing a 3rd party Identity Provider, by using DIDs and Verifiable Credentials using the Presentation Exchange |


# Dissemination

[Sphereon website about PEX & SIOPv2](https://sphereon.com/solution/dif-presentation-exchange-with-siop-v2/)

Demo video using Presentation Exchange with SIOPv2 and OIDC4VP: [Youtube Demo](https://www.youtube.com/watch?v=cqoKuQWPj-s&ab_channel=NielsKlomp)

Socials: [PEX SIOV2 Tweet](https://twitter.com/Sphereon/status/1449772067541430274), [PEX stable release Tweet](https://twitter.com/niels_klomp/status/1481687018493120515), [LinkedIn](https://www.linkedin.com/feed/update/urn:li:activity:6855963698629021697)

# Final words
We would like to thank eSSIF-Lab for the funding, support and connections. Without the grant it is unlikely we would have created an Open-Source component of this size that is not immediately vital for our commercial solutions. We believe our component can and will help in overall interoperability between wallets and verifiers, which is a win for the SSI ecosystem at large.
